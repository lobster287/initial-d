import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { asyncScheduler, of } from 'rxjs';
import { switchMap, map, catchError, debounceTime } from 'rxjs/operators';
import { DataService } from '../../services';
import { PageActions, ReportApiActions } from '../actions';

@Injectable()
export class ReportEffects {
  getReport$ = createEffect(() => ({ debounce = 300, scheduler = asyncScheduler } = {}) =>
    this.actions$.pipe(
      ofType(PageActions.loadFindRecords),
      debounceTime(debounce, scheduler),
      switchMap(({ data }) =>
        this.dataService.getViolationStatistics(data).pipe(
          map((items) =>
            ReportApiActions.loadReportApisSuccess({ data: items })
          ),
          catchError((err) =>
            of(ReportApiActions.loadReportApisFailure({ error: err }))
          )
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private readonly dataService: DataService
  ) {}
}
