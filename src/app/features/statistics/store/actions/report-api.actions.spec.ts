import * as fromReportApi from './report-api.actions';

describe('loadReportApis', () => {
  it('should return an action', () => {
    expect(fromReportApi.loadReportApis().type).toBe('[ReportApi] Load ReportApis');
  });
});
