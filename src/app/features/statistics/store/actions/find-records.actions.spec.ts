import * as fromFindRecords from './find-records.actions';

describe('loadFindRecordss', () => {
  it('should return an action', () => {
    expect(fromFindRecords.loadFindRecordss().type).toBe('[FindRecords] Load FindRecordss');
  });
});
