import { createAction, props } from '@ngrx/store';
import { FilterData } from '../../models';

export const loadFindRecords = createAction(
  '[FindRecords] Load FindRecordss',
  props<{ data: FilterData}>()
);
