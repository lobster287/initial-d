import { createAction, props } from '@ngrx/store';
import { ViolationStatisticRow } from '../../models';

export const loadReportApisSuccess = createAction(
  '[ReportApi] Load ReportApis Success',
  props<{ data: ViolationStatisticRow[] }>()
);

export const loadReportApisFailure = createAction(
  '[ReportApi] Load ReportApis Failure',
  props<{ error: string }>()
);
