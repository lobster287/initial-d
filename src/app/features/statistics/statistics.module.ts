import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StatisticsRoutingModule } from './statistics-routing.module';
import { TableComponent, AppliedFiltersComponent } from './components';
import { PageComponent, FiltersFormComponent } from './containers';
import { DataService, DictionaryService } from './services';

import { TableModule } from 'primeng/table';
import { FormsModule } from '@angular/forms';

import { DropdownModule } from 'primeng/dropdown';
import { ButtonModule } from 'primeng/button';
import { EffectsModule } from '@ngrx/effects';
import { ReportEffects } from './store/effects';

const COMPONENTS = [TableComponent, AppliedFiltersComponent];
const CONTAINERS = [PageComponent, FiltersFormComponent];

@NgModule({
  declarations: [CONTAINERS, COMPONENTS],
  imports: [
    CommonModule,
    StatisticsRoutingModule,
    TableModule,
    FormsModule,
    DropdownModule,
    ButtonModule,
    EffectsModule.forFeature([ReportEffects]),
  ],
  providers: [DataService, DictionaryService],
})
export class StatisticsModule {}
