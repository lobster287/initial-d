import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { FilterData, ViolationStatisticRow } from '../models';

@Injectable({
  providedIn: 'any',
})
export class DataService {
  constructor() {}

  public getViolationStatistics(data: FilterData): Observable<ViolationStatisticRow[]> {
    return of([
      {
        truckCategory: 'testCategory',
        trackExcessThrust: 1234,
        trackExcessGrossWeight: 1234,
        trackExcessDimensions: 1234,
        violationCount: 1234,
        factCount: 1234,
        id: 'test-guid',
      },
    ]);
  }
}
