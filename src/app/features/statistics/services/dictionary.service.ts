export { TruckCategory, DictionaryService };

import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

class TruckCategory {
  public constructor(public readonly name: string, public readonly value: string) {}
}

@Injectable({
  providedIn: 'any',
})
class DictionaryService {
  constructor() {}

  public getTruckCategories(): Observable<TruckCategory[]> {
    return of([
      new TruckCategory('TestName', 'TestValue'),
      new TruckCategory('TestName1234', 'TestValue1234'),
    ]);
  }
}
