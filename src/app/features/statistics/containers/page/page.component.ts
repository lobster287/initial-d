import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { ViolationStatisticRow } from '../../models';
import { DataService } from '../../services';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss'],
})
export class PageComponent implements OnInit {
  public readonly tableItems$: Observable<ViolationStatisticRow[]>;

  constructor(private readonly dataService: DataService) {
    this.tableItems$ = of([]);
  }

  ngOnInit(): void {}
}
