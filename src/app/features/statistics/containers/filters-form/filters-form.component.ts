import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { DictionaryService, TruckCategory } from '../../services';

@Component({
  selector: 'app-filters-form',
  templateUrl: './filters-form.component.html',
  styleUrls: ['./filters-form.component.scss'],
})
export class FiltersFormComponent implements OnInit {
  public readonly truckCategories$: Observable<TruckCategory[]>;
  public selectedTruckCategories: TruckCategory;

  constructor(private readonly dictionaryService: DictionaryService) {
    this.truckCategories$ = this.dictionaryService.getTruckCategories();
  }

  ngOnInit(): void {}

  public find(): void {}

  public clear(): void {}
}
