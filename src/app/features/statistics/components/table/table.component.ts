import { Component, Input, OnInit } from '@angular/core';
import { ViolationStatisticRow } from '../../models';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableComponent implements OnInit {
  @Input() data!: ViolationStatisticRow[];

  constructor() {}

  ngOnInit(): void {}
}
