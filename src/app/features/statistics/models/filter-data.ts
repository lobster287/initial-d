import { TruckCategory } from "../services";

type SelectedUsersFilters = TruckCategory;

export class FilterData {
    constructor(public readonly data: SelectedUsersFilters) {}
}