export interface ViolationStatisticRow {
  truckCategory: string;
  trackExcessThrust: number;
  trackExcessGrossWeight: number;
  trackExcessDimensions: number;
  violationCount: number;
  factCount: number;
  id: string;
}
