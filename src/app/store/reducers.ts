import { InjectionToken } from "@angular/core";
import { ActionReducerMap, Action, MetaReducer } from "@ngrx/store";

export { State, ROOT_REDUCERS, metaReducers };

/**
 * As mentioned, we treat each reducer like a table in a database. This means
 * our top level state interface is just a map of keys to inner state types.
 */
interface State {
    
}

/**
 * Our state is composed of a map of action reducer functions.
 * These reducer functions are called with each dispatched action
 * and the current or initial state and return a new immutable state.
 */
const ROOT_REDUCERS = new InjectionToken<ActionReducerMap<State, Action>>('Root reducers token', {
  factory: () => ({}),
});

const metaReducers: MetaReducer<State>[] = [];